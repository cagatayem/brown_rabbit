
const input = document.getElementById("search-input")
const paragraph =document.getElementById("text").innerHTML
const firstPart = document.getElementById("part-1").innerHTML
const secondPart = document.getElementById("part-2")



input.addEventListener("keyup", e =>{
    const string = e.target.value
    highlightCharacter(string)
})

const splitParagraph = paragraph.split("")
const splitFirstPart = firstPart.split("")

// I have diveded my text into 2 span in order able to search only visible text. Otherwise it display all text
// when typing something in input field

function highlightCharacter(string){
        
        if(secondPart.className.indexOf('hidden') > -1){  // here i check if the text has class hidden in order to search a letter in only visible part of the paragraph
            const firstPartCharacter = splitFirstPart.map(element => {
                return element === string ? `<mark>${element}</mark>`: element
            });
            
            const newParagraph = firstPartCharacter.join("")
            document.getElementById("text").innerHTML =newParagraph
        }
        else{
            const findCharacter = splitParagraph.map(element => {
                return element === string ? `<mark>${element}</mark>`: element
            
            });
            
            const newParagraph = findCharacter.join("")
            document.getElementById("text").innerHTML =newParagraph
        }
}  
    

//  ----- button toggle --------
// I couldn't find why it doesn't work after i type something. I would like to learn as well 


const button = document.getElementById("button")

button.addEventListener("click", ()=>{
    secondPart.classList.toggle("hidden")
    change()
})

function change() {
    button.innerHTML === "Show" ? button.innerHTML="Hide" : button.innerHTML="Show";
}
   



// -----------Changing the first image on refresh-----------

const indicators = document.querySelectorAll(".carousel-item img")

let numbers= []

// Fist of all  i have generated numbers( numbers in image name) randomly and stored them 
    while(numbers.length < 3){
        let rand = Math.floor((Math.random() * indicators.length) + 1)
        if(numbers.indexOf(rand) === -1) numbers.push(rand);
    }


//Then i add these numbers randomly to image names
window.onload =function refresh(){
    for(let i =0 ; i<indicators.length ; i++){
        indicators[i].src = `image-${numbers[i]}.jpg`
    }
}












